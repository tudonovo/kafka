# kafka commands

#### start zookeper
bin/zookeeper-server-start.sh config/zookeeper.properties

#### kafka server start
bin/kafka-server-start.sh config/server.properties

#### create topic
bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic topic_1

#### start consumer
bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic topic_1 --from-beginning 


